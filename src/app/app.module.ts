import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './include/header/header.component';
import { FooterComponent } from './include/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ServicesComponent } from './services/services.component';
import { WorkComponent } from './work/work.component';
import { JobsComponent } from './jobs/jobs.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TeamComponent } from './team/team.component';
import { EventsComponent } from './events/events.component';
import { BirthdayDetailsComponent } from './birthday-details/birthday-details.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { ActafunComponent } from './projects/actafun/actafun.component';
import { CatmangoComponent } from './projects/catmango/catmango.component';
import { FineMasterComponent } from './projects/fine-master/fine-master.component';
import { HomeEaseComponent } from './projects/home-ease/home-ease.component';
import { PlushAffairsComponent } from './projects/plush-affairs/plush-affairs.component';
import { RewillaComponent } from './projects/rewilla/rewilla.component';
import { TripbonderComponent } from './projects/tripbonder/tripbonder.component';
import { TydalpassComponent } from './projects/tydalpass/tydalpass.component';
import { PlaygroundRatersComponent } from './projects/playground-raters/playground-raters.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutUsComponent,
    ServicesComponent,
    WorkComponent,
    JobsComponent,
    ContactUsComponent,
    TeamComponent,
    EventsComponent,
    BirthdayDetailsComponent,
    EventDetailsComponent,
    ActafunComponent,
    CatmangoComponent,
    FineMasterComponent,
    HomeEaseComponent,
    PlushAffairsComponent,
    RewillaComponent,
    TripbonderComponent,
    TydalpassComponent,
    PlaygroundRatersComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
