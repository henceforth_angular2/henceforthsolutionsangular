import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './include/header/header.component';
import { FooterComponent } from './include/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ServicesComponent } from './services/services.component';
import { WorkComponent } from './work/work.component';
import { JobsComponent } from './jobs/jobs.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TeamComponent } from './team/team.component';
import { EventsComponent } from './events/events.component';
import { BirthdayDetailsComponent } from './birthday-details/birthday-details.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { ActafunComponent } from './projects/actafun/actafun.component';
import { CatmangoComponent } from './projects/catmango/catmango.component';
import { FineMasterComponent } from './projects/fine-master/fine-master.component';
import { HomeEaseComponent } from './projects/home-ease/home-ease.component';
import { PlushAffairsComponent } from './projects/plush-affairs/plush-affairs.component';
import { RewillaComponent } from './projects/rewilla/rewilla.component';
import { TripbonderComponent } from './projects/tripbonder/tripbonder.component';
import { TydalpassComponent } from './projects/tydalpass/tydalpass.component';
import { PlaygroundRatersComponent } from './projects/playground-raters/playground-raters.component';




const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'services', component: ServicesComponent },
  { path: 'work', component: WorkComponent },
  { path: 'jobs', component: JobsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'team', component: TeamComponent },
  { path: 'events', component: EventsComponent },
  { path: 'birthday-details', component: BirthdayDetailsComponent },
  { path: 'event-details', component: EventDetailsComponent },
  { path: 'actafun', component: ActafunComponent },
  { path: 'catmango', component: CatmangoComponent },
  { path: 'fine-master', component: FineMasterComponent },
  { path: 'home-ease', component: HomeEaseComponent },
  { path: 'rewilla', component: RewillaComponent },
  { path: 'tripbonder', component: TripbonderComponent },
  { path: 'tydalpass', component: TydalpassComponent },
  { path: 'playground-raters', component: PlaygroundRatersComponent },
  { path: 'plush-affairs', component: PlushAffairsComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
